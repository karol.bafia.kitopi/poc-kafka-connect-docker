# Ready to use solution for streaming DB changes to Kafka. 
The goal of this project: create a complete solution for tailing Mongo and Postgres DB log (opLog/transaction log) and pushing it to Kafka.
The main purpose is building a Transactional Outbox pattern on top of this, but it is not limited only to this. It can be used for any sort of task where streaming DB changes to Kafka is needed.
## What is delivered?
Implementation for Mongo and Postgres using Self-Managed Kafka Connect (wrapped by strimzi): `strimzi/kafka-connect`. (This is an alternative to AWS MSK cloud-managed Connector.)

As with our other projects, configuration is with Helm Charts, logs are available on Kibana and monitoring is available on Grafana.

Kafka-Connect  [Official Doc](https://docs.confluent.io/platform/current/connect/)

## How to configure?
1. Kafka-Connect internal topics (see below ["Workers and Clusters"](#workers-and-clusters)) and the topic to which DB is streamed, should be created using Terraform.
2. In values.yaml (for Helm Chart) define DB name (password will be resolved automatically) and Kafka topics names from the previous point.
3. (If needed we can add [schema validation](https://docs.confluent.io/platform/current/connect/userguide.html#sr-long).) 

### Sample configuration (kafka-topics without Outbox yet)
* https://gitlab.com/kitopi-com/infrastructure/terraform/environments-main/-/merge_requests/1057/diffs
* https://gitlab.com/kitopi-com/infrastructure/terraform/environments-main/-/merge_requests/1065/diffs
* https://gitlab.com/kitopi-com/infrastructure/terraform/variables/-/merge_requests/907/diffs
* https://gitlab.com/kitopi-com/infrastructure/terraform/environments-kafka-topics/-/merge_requests/159/diffs
* https://gitlab.com/kitopi-com/infrastructure/eks-clusters/-/merge_requests/1480/diffs
* https://gitlab.com/kitopi-com/skos/dine-in-api/-/merge_requests/1100/diffs

## Background needed to configure
### Providing High Availability
As in the case of our other Kubernetes project, there will be 3 Kafka-Connect Workers (Docker containers) replicas on production. They form fault-tolerant clusters. Each Kafka-Connect container/worker has 1 connector (it tails a DB log) with 1 associated task (it copies a connector’s data, so they can be sent to the Kafka topic).


<a name="workers-and-clusters"></a>
### Workers and Clusters
Each docker container is a worker. Workers will form a cluster if the following properties of a worker are the same (since teams define this only once, no extra work is needed to ensure this):
 - `CONNECT_GROUP_ID`
 - `CONNECT_CONFIG_STORAGE_TOPIC`
 - `CONNECT_OFFSET_STORAGE_TOPIC`
 - `CONNECT_STATUS_STORAGE_TOPIC`

Group id should include DB we connect to eg `dine-in-connect-cluster-group`.\
Topic names should be similarly prefixed, eg:
- `dine-in-connect-configs`
- `dine-in-connect-offset`
- `dine-in-connect-status`

The 3 internal topics above are where kafka-connect stores shared connector and task configurations, offsets, and status (more [here](https://docs.confluent.io/platform/current/connect/userguide.html#kconnect-internal-topics)).\
We manually create those topics in TF (kafka-connect can also create them automatically - not our case). Copy example TF code from  an existing configuration. The following [guidelines](https://docs.confluent.io/platform/current/connect/userguide.html#manually-create-internal-topics) are applied there:
- `CONNECT_OFFSET_STORAGE_TOPIC`: always create it as a compacted, highly replicated (3x or more) topic with a large number of partitions (25-50 if large Kafka-Connect cluster but this is not our case, so 4-8 should be fine for us)
- `CONNECT_STATUS_STORAGE_TOPIC`: always create it as a compacted, highly replicated (3x or more) topic with multiple partitions.
- `CONNECT_CONFIG_STORAGE_TOPIC`: create it as a compacted, highly replicated (3x or more). Must always have exactly one partition (this is why there is even no such property if topic is automatically created).  

### Tasks
Each worker will execute 1 task for Mongo or 1 task for Postgres. (We plan to start with 1 cluster per each project's DB.)\
Each task has an associated DB *connector*. It is configured together with a DB connection. Notice the`"tasks.max":"1"` property. Why there is only 1 task for a connector?:
- For Mongo: "*The Source connector still only supports a single task, this is because it uses a single Change Stream cursor. This is enough to watch and publish changes cluster wide, database wide or down to a single collection.*" See this [forum](https://www.mongodb.com/community/forums/t/kafka-connector-task-spawn-strategy/6443/2) post.
- For Postgres Source: "*The PostgreSQL connector always uses a single task and therefore does not use this value*". See Debezium [doc](https://debezium.io/documentation/reference/2.4/connectors/postgresql.html#postgresql-connector-properties)

## Tuning (adjusting to events volume)
### Tuning Mongo connector
Upon this [article](https://www.mongodb.com/developer/products/connectors/tuning-mongodb-kafka-connector/#:~:text=what%20to%20expect%20when%20changing%20these%20values):
1. The Kafka Source Connector issues `getMore` commands to the source cluster using `batch.size` (default **1'000**). 
2. The Kafka Source Connector receives the results from step 1 and waits until either `poll.max.batch.size` (default **1'000**) or `poll.await.time.ms` (default **5'000**) is reached. While this doesn't happen, the Kafka Source Connector keeps “feeding” itself with more `getMore` results. 
3. When either `poll.max.batch.size` (1k) or `poll.await.time.ms` (5s) is reached, and the source records are sent to Kafka.

So with these defaults, if we have less than 1000 events per 5s, the lag between DB change and posting it to Kafka can reach 5s. With low events' volume, both properties' values should be decreased to reduce lag.

(In the article we will find a lot of information about tuning `copy.existing=true`, currently named `startup.mode=copy_existing`. It tells the connector to copy all existing source data to Change Stream events. This is not applicable for the outbox case, and might not be related to any of our cases. So can be ignored.)

## Monitoring
Ready to use Grafana will be delivered to the teams.
[Prometheus dev](https://p8s.k8s.dev.kitopiconnect.com/graph?g0.expr=kafka_mongodb_source_getmore_commands_successful&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h) sample metric.\


Be aware that all kafka connect itself and DB connectors metrics are wrapped and forwarded by
[strimzi](https://github.com/strimzi/strimzi-kafka-operator/blob/main/examples/metrics/kafka-connect-metrics.yaml). What means they get prefixes listed in sections below.

### Kafka Connect metrics
[https://docs.confluent.io/platform/current/connect/monitoring.html#source-task-metrics](https://docs.confluent.io/platform/current/connect/monitoring.html#source-task-metrics "smart-link") :

**Common task metrics**

MBean: **kafka.connect:type=connector-task-metrics,connector="{connector}",task="{task}"**

* status
* offset-commit-failure-percentage

In our [Prometheus](https://p8s.k8s.dev.kitopiconnect.com/) those metrics are prefixed: `kafka_connect_connector_`


**Source task metrics**

MBean: **kafka.connect:type=source-task-metrics,connector=(\[-.w\]),task=(\[d\])**

* source-record-poll-total
* source-record-write-total
* poll-batch-max-time-ms

In our [Prometheus](https://p8s.k8s.dev.kitopiconnect.com/) those metrics are prefixed:  
`kafka_connect_source_task_`

**Task error metrics**

MBean: **kafka.connect:type=connector-task-metrics,connector="{connector}",task="{task}"**

*   total-record-failures
*   (we will not monitor DLQ bc it is [only for the Sink connector](https://docs.confluent.io/platform/current/connect/index.html#dead-letter-queue))

### Mongo DB Metrics
#### Prometheus & Atlas
[https://www.mongodb.com/docs/atlas/tutorial/prometheus-integration/#procedure](https://www.mongodb.com/docs/atlas/tutorial/prometheus-integration/#procedure "smart-link") & [KITOPI Prometheus](https://p8s.k8s.dev.kitopiconnect.com/graph?g0.expr=%7Binstance%3D~%22scrape-management-api.*%22%7D&g0.tab=1&g0.stacked=0&g0.show_exemplars=0&g0.range_input=3h)

However, I don’t see “Oplog GB/Hour” & “Replication Oplog Window” metrics exposed for Prometheus. So those to need to be monitored on Atlas directly.

#### Atlas' OpLog Monitoring

[https://debezium.io/documentation/reference/connectors/mongodb.html#mongodb-optimal-oplog-config](https://debezium.io/documentation/reference/connectors/mongodb.html#mongodb-optimal-oplog-config "smart-link") Although this is Debezium’s doc, the same applies to any connector that does opLog tailing.

We should monitor the values of (not exposed to Prometheus):

*   _Oplog GB/Hour_ (This refers to the average rate of gigabytes of oplog the primary generates per hour. High unexpected volumes of oplog might indicate a schema design issue or highly insufficient write workload.) May be needed to\[ set the maximum oplog size|https://www.mongodb.com/docs/manual/core/replica-set-oplog/] to: “Oplog GB/Hour X average reaction time to Debezium failure"
*   “_Replication Oplog Window_” (the approximate number of hours available in the primary's replication oplog). If the connector “is offline for an interval that exceeds the value of the replication oplog window, and the primary oplog grows faster than connector can consume entries, a connector failure can result.”

For example, if we take dine-in’s case, where we will deploy the Kafka-Connect solution on a pilot basis, we have ~16 days time window (retention of the oplog).

[prod atlas dine-in monitoring](https://cloud.mongodb.com/v2/6214c558276f9a407725482f#/host/replicaSet/62a07b98d52a1c77ce7ae687)

2 days should be enough, so big reserve. However, if logic changes and suddenly this time is shortened below 1 day, we might not be able to react. This is why we should consider setting up an Alert for the “_Replication Oplog Window_” value, if it drops below 1day. See [https://www.mongodb.com/docs/atlas/alerts/](https://www.mongodb.com/docs/atlas/alerts/)

It looks like we don’t need to alert on “Oplog GB/Hour” bc that metric occurs in the counter phase:

[prod atlas crm-bridge monitoring](https://cloud.mongodb.com/v2/6214c558276f9a407725482f#/host/replicaSet/63048e7673d0521b923c34da)

### MongoDB Kafka Connector metrics
*The MongoDB Kafka Connector and Kafka Connect both produce metrics for MongoDB connector tasks.*
[Official doc](https://www.mongodb.com/docs/kafka-connector/current/monitoring/).

"*Both sets of metrics provide information about how your tasks interact with Kafka Connect, but only the MongoDB Kafka Connector metrics provide information about how your tasks interact with MongoDB.*"  
([Monitoring: JMX paths for Kafka-connect & mongo connector](https://www.mongodb.com/docs/kafka-connector/current/monitoring/#jmx-paths)).

Mongo-Kafka-Connector metrics [Mongo connector monitoring](https://www.mongodb.com/docs/kafka-connector/current/monitoring/#source-connector) we can start with:

*   `latest-mongodb-time-difference-secs`
*   `records`
*   `(records - (records-acknowledged + records-filtered)) / records`
*   `in-task-poll-duration-ms` vs `in-connect-framework-duration-ms`
*   `getmore-commands-failed`

In our [Prometheus](https://p8s.k8s.dev.kitopiconnect.com/) those metrics are prefixed:  
`kafka_mongodb_source_`
---


> The sections below are related to the development of the solution. 
> Teams who are only using can stop reading here.

## Transactional Outbox
The Outbox pattern implementation is not yet provided.
This section explains how the Outbox pattern implementation can be achieved.
### Creating Kafka message out of the Outbox record
The goal we want to achieve by introducing the Outbox pattern is to replace existing non-transactional (not atomic) Kafka publications (no transaction between DB and Kafka) with that pattern. Those publications are already present in all environments, including production, so we need a seamless transition. That means we need to reuse Kafka message mappers for Java classes, including custom serializers, to create the message's payload. We also need to reuse existing distributed tracing ID propagation mechanisms (B3, W3C, Dynatrace), to create message headers. Also, the payload's class name goes to the header.\
Tracing IDs as well as serialized class names can be found in the Kafka message's headers, eg:
```
X-dynaTrace: x?jc�~@LJ�7������9�  .dB����׎�pQ��iLI�
b3: ad26a65221e2c7ce-dafe51e64f925c35-0
__TypeId__: com.kitopi.area86.BranchChangedEvent
```
The serialized object's body goes to the message's payload. For example (serialized class BranchChangedEvent):
```
{
  "correlationId": "e00a916a-612e-4879-8f90-0d5da233256d",
  "occurredOn": "2023-10-30T11:44:34.201956757Z"
}
```
Out of the box implementations of the Transactional Outbox pattern that Debezium provides (see [Debezium MongoDB connector](https://debezium.io/documentation/reference/2.4/transformations/mongodb-outbox-event-router.html)) will not allow this.

A custom implementation for serializing both, object's class name and object itself needs 
to be provided.
One of the ideas is to call the same Kafka's logic which is used for message's producing. You can put a debug breakpoint on `KafkaProducer.send: 330` and start Kafka adapter integration test to see how it is currently done and how could we  potentially hook inside that logic.

### Outbox for MongoDB
There are some solutions available which can help us with the pattern implementation:
- [Option for mapping OUTBOX collection records to a Kafka topic](https://www.mongodb.com/docs/kafka-connector/current/source-connector/usage-examples/topic-naming/#topic-namespace-map)
- [Option for providing Distributed Tracing](https://docs.confluent.io/platform/current/connect/transforms/headerfrom.html).  This [transformer](https://docs.confluent.io/platform/current/connect/index.html#transforms) will also allow moving payload's java class name to the header 

Notice that, in case of log tailing (CDC) connectors, we can INSERT and instantly DELETE a record from the OUTBOX collection. This will be visible in the opLog. We don't care about what is in the collection. This way the collection will always be empty, and we do not need to clean it periodically.  

### Outbox for Postgres in CDC Connector scenario
There are some solutions available which can help us with the pattern implementation:
- [Option for mapping OUTBOX collection records to a Kafka topic](https://debezium.io/documentation/reference/stable/transformations/topic-routing.html)
- Same as above: [Option for providing class name and Distributed Tracing as a header](https://docs.confluent.io/platform/current/connect/transforms/headerfrom.html).

(see also below [Debezium PostgreSQL CDC Connector](#debezium-postgresql-cdc-connector), where out of the box pattern implementation is referenced. Maybe it can provide further inspirations.) 

### Outbox for Postgres in JDBC Connector scenario
For the Outbox pattern case, we need to periodically clean up the OUTBOX table. The question is how often to empty the OUTBOX table, to not lose any records, and to not allow the table to grow too much (VACUUM cost to be considered).
(In the case of the CDC/log tailing approach, we can INSERT and instantly DELETE a record on the OUTBOX table).

## Connectors
### Mongo Connector alternatives
>This section is only for internal information. Not related to any tasks teams need to accomplish.
#### The official MongoDB Kafka CDC Connector
(used in this project)\
It is log-tailing (CDC) kind of connector (or "Massage Releay", as it is called in the design pattern definition).\
Support: Officially supported by MongoDB. (Available also as fully managed on Confluent Cloud.)\
[Official Plugin on Confluent](https://www.confluent.io/hub/mongodb/kafka-connect-mongodb)\
[Official Doc on Mongo](https://www.mongodb.com/docs/kafka-connector/current/)\
[Tuning](https://www.mongodb.com/developer/products/connectors/tuning-mongodb-kafka-connector/)
#### Debezium MongoDB CDC Connector
(consider using)\
It is log-tailing (CDC) kind of connector.\
Support: Supported by Confluent as part of a Confluent Platform subscription.\
[Plugin on Confluent](https://www.confluent.io/hub/debezium/debezium-connector-mongodb)\
[Documentation](https://docs.confluent.io/kafka-connectors/debezium-mongodb-source/current/overview.html)\
See [When things go wrong](https://debezium.io/documentation/reference/connectors/mongodb.html#mongodb-when-things-go-wrong) doc  to understand better risks of CDC approach.

Debezium connector provides also a transformer for mapping OUTBOX collection records to a Kafka topics: [link](https://debezium.io/documentation/reference/2.4/transformations/mongodb-outbox-event-router.html#mongodb-outbox-event-router) (also available as a standalone transformer on confluent hub: [link](https://docs.confluent.io/platform/current/connect/transforms/topicregexrouter.html)), which however might not be suitable for our case of migrating existing asynchronous communication.
We also get from Debezium out of the box solution for Distributed Tracing through the OpenTelemetry specification. It can also be used for Postgres case. But it will also not help us bc of the need for migrating existing Kafka communication.    [Link](https://debezium.io/documentation/reference/stable/integrations/tracing.html)

### PostgreSQL Connector alternatives
> This section is only for internal information. Not related to any tasks teams need to accomplish.
#### Debezium PostgreSQL CDC Connector
(used in this project)\
Support: Supported by Confluent as part of a Confluent Platform subscription.\
[Debezium Postgres Kafka Connect - Official blog with GitHub project](https://debezium.io/blog/2019/02/19/reliable-microservices-data-exchange-with-the-outbox-pattern/). It contains a complete solution with OUTBOX DB table, which however might not be suitable for our case of migrating existing asynchronous communication.

Further details on installing and configuring this connector are [here](#debezium-postgres-configuring)

#### JDBC Connector
(consider using)\
https://www.confluent.io/hub/confluentinc/kafka-connect-jdbc

https://docs.confluent.io/kafka-connectors/jdbc/current/source-connector/overview.html

Can be a far simpler option to implement and maintain than the log-tailing (CDC) alternative. We can consider using it if a lag between updating a DB table (e.g. OUTBOX) and publishing on Kafka is acceptable. I mean here a 1 or several seconds lag, bc that could be the interval on which the connector is polling a DB table. Also, the rate at which records are updated should be similar. (Lags for the CDC approach can also appear.)

## License
Looks like we can use a free license bc Monogo and Debezium connectors are open-source:\
https://docs.confluent.io/platform/current/connect/license.html
&\
https://www.confluent.io/product/connectors/

## Logs
Same as for other apps: Elastic stack.

<a name="debezium-postgres-configuring"></a>
## PostgreSQL connector installing and configuring  
> Internal section. Teams don't need to bother.
#### Debezium connector
##### How it works
The Debezium connector relies on and reflects the PostgreSQL [logical decoding](https://www.postgresql.org/docs/current/logicaldecoding-explanation.html#LOGICALDECODING-EXPLANATION-LOG-DEC) feature  (logical replication).\
PostgreSQL [logical replication](https://www.postgresql.org/docs/current/logical-replication.html) constructs a stream of logical data modifications from the WAL (transactional log).\
Through the [logical decoding interface](https://www.postgresql.org/docs/current/logicaldecoding.html), third-party extensions can subscribe to data modifications.\
As of version 10 of PostgreSQL, the logical replication is supported natively (prior that, the '`pglogical`' plugin was needed.)

**Warning**: Logical replication can consume all DB's disk space if misused. It happens if WAL can't be recycled. ([details](https://www.postgresql.org/docs/current/logicaldecoding-explanation.html#LOGICALDECODING-REPLICATION-SLOTS)). Debezium has a solution for this called "Heart beat".

##### WAL size controlling and Heart-beat
Debezium's Heart-beat feature needs to be used if records in the OUTBOX DB table are inserted much slower than WAL is growing (other tables or other DBs on same instance grow faster).\
If it is so the [Heart-beat](https://debezium.io/documentation/reference/2.4/connectors/postgresql.html#postgresql-wal-disk-space) needs to be configured.

**IMPORTANT**: When running Postgres on AWS RDS, there are some automated maintenance operations executed which appear in WAL as well.
They are impacting WAL growth (learned from Przemek Rożek). What this can be? Not clear, but I found this: If we go to `RDS > Performance Insights > Top databases` in AWS console,
we will see the `rdsadmin` database and user. The AWS' documentation states:
```
rdsadmin – A role that's created to handle many of the management tasks (...). 
This role is used internally by RDS for PostgreSQL for many management tasks.
```
That means we always need hear-beat to be configured.
TODO: Before going to production we must test that hear-beat feature really works. Otherwise, custom heart-beat will be needed.\
**IMPORTANT**: Debezium documentation seams to be incomplete regarding heart-beat, see: [Heart-beat parameter missing configuration](https://groups.google.com/g/debezium/c/39mmGEHii_8).
BTW This problem is not confirmed here: [link](https://debezium.io/blog/2020/02/25/lessons-learned-running-debezium-with-postgresql-on-rds/),
where they claim no heart-beat in needed on RDS.

There is one more important hint in this article regarding WAL monitoring:
```
add alarms on the RDS metric >>>TransactionLogsDiskUsage<<< and >>>OldestReplicationSlotLag<< to alert us when the transaction logs disk usage 
increased above a threshold or when a replication slot started lagging - meaning that Debezium might have died.
```
If we go to our AWS RDS console, to the `Databases > Monitoring > CloudWatch` you will see those metrics there and 2 other also related:
- TransactionLogsDiskUsage
- OldestReplicationSlotLag (The lagging size of the replica lagging the most in terms of write-ahead log (WAL) data received.)
- ReplicationSlotDiskUsage
- TransactionLogsGeneration

Another hint is described in the last section of this [post](https://www.morling.dev/blog/can-debezium-lose-events/)\
"*track the retained WAL size of a replication slot using the pg_current_wal_lsn() and pg_wal_lsn_diff()*"\
From AWS RDS doc:
```
You can also check that the LSN on the replica is increasing with the command select pg_current_wal_lsn();
```
LSN is Log Sequence Number. It is basically a pointer to a location in the WAL.\
but maybe this the same sort of insight as provided by the OldestReplicationSlotLag?

Our plans regarding Postgres setup on AWS RDS:
We will migrate to this setup: 1 RDS is 1 Postgres, which host 1 DB, so we have 1 WAL per DB/RDS.
We will also migrate from ver 11 to 15.

##### on AWS RDS
[Official doc for connecting Debezium to Postgres an AWS](https://debezium.io/documentation/reference/1.0/connectors/postgresql.html#amazon-rds)

##### Postgres failure
*"As of release 12, PostgreSQL allows logical replication slots only on primary servers. This means that you can point a Debezium PostgreSQL connector to only the active primary server of a database cluster. Also, replication slots themselves are not propagated to replicas. If the primary server goes down, a new primary must be promoted."*\
However:\
*"AWS RDS implements replication to a standby via disk replication. This means that the replication slot does get replicated and will remain available after a failover."* [source](https://debezium.io/documentation/reference/2.4/connectors/postgresql.html#postgresql-cluster-failures)
(TODO with Devops clarify if above means that we are on safe side?)

##### Postgres replication-related properties tuning
https://www.postgresql.org/docs/current/runtime-config-replication.html#RUNTIME-CONFIG-REPLICATION-SENDER
(TODO prepare by Karol & ops)

##### Upgrading Postgres
https://debezium.io/documentation/reference/2.4/connectors/postgresql.html#upgrading-postgresql

##### Impact on PostgreSQL performance
Some attempt to assess Debezium's (which uses logical replication) impact on DB performance:
[Benchmarking four Different Replication Solutions](https://www.postgresql.eu/events/pgconfde2022/sessions/session/3773/slides/297/benchmarking_four_replication_solutions-julian_markwort-pgconf_de-2022-05-13.pdf):
*“asynchronous replication has barely any performance cost; performance is best with in-core (built in) streaming (streaming replication); in-core (built in) logical and pglogical (plugin) very similar”*